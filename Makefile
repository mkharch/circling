
PROJ_NAME := circling

.PHONY: default

default:
	rebar compile

build:
	rebar compile
	rebar ling-build

image:
	rebar ling-image
	rsync vmling dom0::images/$(PROJ_NAME).img

me:
	rebar compile
	rebar ling-build-image
	rsync vmling dom0::images/$(PROJ_NAME).img

em:
	rebar compile
	rebar ling-build-image
	rsync vmling dom0::images/$(PROJ_NAME).img

