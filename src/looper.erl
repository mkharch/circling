-module(looper).
-compile(export_all).


start(N) ->

	Writ = <<"Hey there, cowboy!">>,

	Self = self(),

	First = spawn(fun() ->
		next()
	end),

	First ! {l,Writ,Self,0,N},

	receive
	{l,NewWrit,Passed} ->
		io:format("Message ~p passed ~w processes~n", [NewWrit,Passed])
	end.

next() ->
	io:format("~p starts~n", [self()]),
	receive
	{l,Writ,First,NP,0} ->
		First ! {l,Writ,NP +1};
	{l,Writ,First,NP,Left} ->
		Next = spawn(fun() ->
			next()
		end),
		Next ! {l,Writ,First,NP +1,Left -1}
	end,
	io:format("~p exits~n", [self()]).

