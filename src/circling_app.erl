-module(circling_app).

-behaviour(application).

-define(SEED_ADDR, {192,168,2,2}).
-define(PEER_MIN, 3).
-define(PEER_MAX, 254).

-define(NAME_PREFIX, "circling").

-define(CIRCLING_IMAGE, <<"/root/images/circling.img">>).
-define(GATOR_HOST, {192,168,0,1}).
-define(BRIDGE, <<"br2">>).

-define(ZERO_HOST, {192,168,0,2}).
-define(ZERO_PORT, 9010).

-define(LOCAL_HOPS, 7777).

%% Application callbacks
-export([start/2, stop/1]).

-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
	application:start(circling).

start(_StartType, _StartArgs) ->
	egator_edge:start_link(),

	{ok,[[AddrStr]]} = init:get_argument(ipaddr),
	{ok,[[DomStr]]} = init:get_argument(domain),
	Domain = list_to_binary(DomStr),

	{ok,MyAddr} = inet_parse:address(AddrStr),
	?NAME_PREFIX ++ MyIdStr = DomStr,
	PeerId = list_to_integer(MyIdStr) +1,
	PeerAddr = next_addr(MyAddr),
	PeerName = list_to_binary(io_lib:format("~s~w", [?NAME_PREFIX,PeerId])),

	Sink = case init:get_argument(seed) of
	{ok,[[SeedTotalStr,MessageStr]]} ->

		%% seed instance

		%%%%
		zero_notify({struct,[{domain,Domain},
							 {event,<<"seeding">>},
							 {my_addr,list_to_binary(AddrStr)}]}),
		%%%%

		Started = now(),

		io:format("Spawning the first peer...~n", []),
		PeerAddr = spawn_peer(PeerName, PeerAddr),
		{A,B,C,D} = PeerAddr,

		%%%%
		zero_notify({struct,[{domain,Domain},
							 {event,<<"spawned">>},
							 {peer_name,PeerName},
							 {peer_addr,list_to_binary(inet_parse:ntoa(PeerAddr))}]}),
		%%%%

		io:format("Mounting the first peer [~w.~w.~w.~w]...~n", [A,B,C,D]),
		mount_peer(Domain, PeerName, PeerAddr),

		Message = list_to_binary(MessageStr),

		SeedTotal = list_to_integer(SeedTotalStr),
		io:format("Dispatching the first message [~w]...~n", [SeedTotal]),
		peer_message(Domain, PeerName, {hi,Message,MyAddr,0,0,SeedTotal}),

		spawn(fun() ->
			seed_sink(Domain, Started)
		end);

	error ->
		
		%% ordinary instance

		%%%%
		zero_notify({struct,[{domain,Domain},
							 {event,<<"starting">>},
							 {my_addr,list_to_binary(AddrStr)}]}),
		%%%%

		spawn(fun() ->
			regular_sink(Domain, PeerName, PeerAddr)
		end)
	end,

	ok = '9p_server':add_export(<<"sink">>, circle_export, Sink),
	ok = '9p_server':add_listener(circle, '9p_tcp', {{0,0,0,0},564}),
	%'9p_server':trace_messages(true),

	%%%%
	zero_notify({struct,[{domain,list_to_binary(DomStr)},
						 {event,<<"ready">>},
						 {my_addr,list_to_binary(AddrStr)}]}),
	%%%%

	%% the parent can mount us
	io:format("Ready for external mounts...~n", []),
	egator_edge:signal_ready(),

    circling_sup:start_link().

stop(_State) ->
    ok.

%%------------------------------------------------------------------------------

seed_sink(Domain, {G1,S1,U1}) ->
	io:format("Waiting for the loop to complete...~n", []),
	receive X -> X end,
	{done,Message,NN,NP} = X,
	{G2,S2,U2} = now(),
	Elapsed = (G2 -G1) *1000000.0 +(S2 -S1) +(U2 -U1) /1000000.0,
	io:format("Circle complete:~n", []),
	io:format("~w node(s)~n~w process(es)~n~.2f seconds~n", [NN,NP,Elapsed]),

	%%%%
	zero_notify({struct,[{domain,Domain},
						 {event,<<"complete">>},
						 {message,Message},
						 {elapsed,trunc(Elapsed *1000)},
						 {processes,NP},
						 {nodes,NN}]}),
	%%%%

	ok.

regular_sink(Domain, PeerName, PeerAddr) ->
	receive
	{hi,Message,SeedAddr,NN,NP,0} ->
		mount_peer(Domain, PeerName, SeedAddr),
		peer_message(Domain, PeerName, {done,Message,NN +1,NP +1});

	{hi,Message,SeedAddr,NN,NP,Left} ->

		Message1 = local_ring(Message, ?LOCAL_HOPS),

		PeerAddr = spawn_peer(PeerName, PeerAddr),
		mount_peer(Domain, PeerName, PeerAddr),
		peer_message(Domain, PeerName,
				{hi,Message1,SeedAddr,NN +1,NP +?LOCAL_HOPS,Left -1})
	end,

	receive after 3000 -> ok end,

	zero_notify({struct,[{domain,Domain},
						 {event,<<"destroying">>}]}),
	%%
	%% init:stop() is not enough. We are not connected to console and the domain
	%% will not be destroyed automatically.
	%%
	egator:destroy(Domain, [{host,?GATOR_HOST}]). 

spawn_peer(PeerName, PeerAddr) ->
	Ticket = egator_edge:new_ticket(),
	ReadySpec = egator_edge:extra_spec(Ticket),

	Extra = list_to_binary(io_lib:format("~s -domain ~s -ipaddr ~s "
			"-netmask 255.255.255.0 -gateway 192.168.2.1 "
			"-root /erlang -pz /circling/ebin -pz /circling/deps/egator/ebin "
			"-home /circling -s circling_app",
					[ReadySpec,PeerName,inet_parse:ntoa(PeerAddr)])),

	_Ok = egator:create(PeerName, ?CIRCLING_IMAGE,
			[{memory,32},{extra,Extra},{bridge,?BRIDGE}], [{host,?GATOR_HOST}]),

	ready = egator_edge:wait_for_edge(Ticket),
	PeerAddr.

next_addr({A,B,C,D}) when D < ?PEER_MIN; D > ?PEER_MAX ->
	{A,B,C,?PEER_MIN};
next_addr({A,B,C,D}) when D +1 =< ?PEER_MAX ->
	{A,B,C,D +1};
next_addr({A,B,C,_}) ->
	{A,B,C,?PEER_MIN}.

mount_peer(Domain, PeerName, PeerAddr) ->
	ok = '9p_mounter':add_connection('9p_tcp', {PeerAddr,564},
				[{<<"/peer">>,<<"/sink">>}], [sync]),
	zero_notify({struct,[{domain,Domain},
						 {event,<<"mounted">>},
						 {peer_name,PeerName}]}).

peer_message(Domain, PeerName, Msg) ->
	ok = file:write_file("/peer/mailbox", term_to_binary(Msg)),
	zero_notify({struct,[{domain,Domain},
						 {event,<<"sent">>},
						 {peer_name,PeerName}]}).

zero_notify(Json) ->
	Packet = list_to_binary(circle_json:encode(Json)),
	{ok,Tmp} = gen_udp:open(0),
	ok = gen_udp:send(Tmp, ?ZERO_HOST, ?ZERO_PORT, Packet),
	ok = gen_udp:close(Tmp).

local_ring(Message, N) ->
	Peer = spawn(fun() ->
		juggle()
	end),

	Peer ! {self(),Message,N},
	receive
	{done,Message1} -> Message1
	end,

	Message1.

juggle() ->
	receive
	{Top,Message,0} ->
		Top ! {done,Message};
	{Top,Message,N} ->
		Peer = spawn(fun() ->
			juggle()
		end),
		Peer ! {Top,Message,N -1}
	end.

%%EOF
